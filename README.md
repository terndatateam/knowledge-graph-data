# TERN Knowledge Graph Data

**Document status**: *DRAFT*

This repository holds the point-of-truth for TERN-curated controlled vocabularies, descriptions of TERN-affiliated persons and organisations, and links to the various projects and catalogue metadata for TERN's data-driven applications. 

This repository is the parent repository holding the shared tools and governance procedures for creating and modifying TERN RDF data. This repository will hold Git submodules to smaller repositories for easier maintenance and cleaner commit logs. Git submodules will also enable separate pipelines workflow for each type of RDF data. E.g. controlled vocabularies for plot-based data may want to ingest into the triplestore (database for RDF data) as well as Research Vocabularies Australia's (RVA) portal for vocabulary discovery.  

*This repository does not include the large RDF datasets from the ecological data platform.*

## What is RDF?

The Resource Description Framework (RDF) is a standard data model for data interchange on the World Wide Web. The RDF data model stores data in the form of RDF statements, which are commonly known as a set of triples. A triple is made up of three parts, the *subject*, the *predicate* and the *object*. 

<subject> <predicate> <object> .

Let's see how this looks like when representing an organisation.

```
<https://www.tern.org.au> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://schema.org/Organization> .
<https://www.tern.org.au> <http://schema.org/name> "Terrestrial Ecosystem Research Network" .
<https://www.tern.org.au> <http://schema.org/email> "tern.data@uq.edu.au" .
<https://www.tern.org.au> <http://schema.org/address> "Goddard Building, The University of Queensland, St Lucia QLD 4072, Australia" .
```

Each line is an RDF statement with a set of triples. Every class and property in RDF is represented by a URI [13]. Let's introduce some CURIE's [12] and make the statements more readable. 


```
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix sdo: <http://schema.org/> .

<https://www.tern.org.au> rdf:type sdo:Organization .
<https://www.tern.org.au> sdo:name "Terrestrial Ecosystem Research Network" .
<https://www.tern.org.au> sdo:email "tern.data@uq.edu.au" .
<https://www.tern.org.au> sdo:address "Goddard Building, The University of Queensland, St Lucia QLD 4072, Australia" .
```

The prefixes followed by a colon (`rdf:`, `sdo:`) are a part of the CURIE syntax.[12] For example, not using a CURIE for `sdo:name` would result in the fully qualified URI `http://schema.org/name`. You can see the fully qualified URI's defined with the `@prefix` at the top of the snippet. Now it is very easy to read. We can read each line like a sentence in English. 

```
TERN is type Organization.
TERN has name Terrestrial Ecosystem Research Network.
TERN has email tern.data@uq.edu.au.
TERN has address Goddard Building, The University of Queensland, St Lucia QLD 4072, Australia.
```

With the Turtle syntax, we can make the above RDF even more compact.

```
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix sdo: <http://schema.org/> .

<https://www.tern.org.au> rdf:type sdo:Organization ;
    sdo:name "Terrestrial Ecosystem Research Network" ;
    sdo:email "tern@uq.edu.au" ;
    sdo:address "Goddard Building, The University of Queensland, St Lucia QLD 4072, Australia" .
```

Now we no longer need to repeat the *subject* on each line. Do note the "block" of statements are terminated by a period (full stop), and not a semicolon. 

The predicate is generally known as the *property* or the *relationship* of a subject. A subject can be in the place of an object and vice versa but the predicate must always be in the same position in an RDF statement. So far, our predicates have only linked to strings. Other literal values are numbers, dates and time, and booleans. Let's see how we can link some other entity to the organisation. 

Example of connecting a person to an organisation.

```
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix sdo: <http://schema.org/> .

<https://www.tern.org.au> rdf:type sdo:Organization ;
    sdo:name "Terrestrial Ecosystem Research Network" ;
    sdo:email "tern@uq.edu.au" ;
    sdo:address "Goddard Building, The University of Queensland, St Lucia QLD 4072, Australia" .

<https://www.tern.org.au/person/edmond> rdf:type sdo:Person ;
    sdo:name "Edmond Chuc" ;
    sdo:email "e.chuc@uq.edu.au" ;
    sdo:affiliation <https://www.tern.org.au> .
```

You can see on the last line, the person `<https://www.tern.org.au/person/edmond>` has an affiliation to `<https://www.tern.org.au>`. This is how we link entities to other entities in RDF. 

Here is a diagram of what the data looks like visually.

![Organisation and Person relationship diagram](docs/img/org-person-diagram.png)

The above example of linking a person to an organisation demonstrates how to link different entities to each other. This linking is what allows us to create a graph of inter-connected data, which we will call the *TERN Knowledge Graph*. 

The RDF model can be serialised in many different forms: Turtle, RDF+XML, JSON-LD, N-Triples, etc. All are parsable by machines, but the most human-friendly syntax is Turtle. 


## What are Ontologies and Vocabularies?

Ontologies are semantic data models to describe domain-specific information. It enables the sharing and reuse of domain knowledge across disciplines. These data models are sometimes also referred to as *vocabularies* as they are a set of standardised terminology to describe *something*. In the Semantic Web, OWL is designed to describe *things*, *groups of things*, and the relationships between *things*. Ontologies are defined through a set of classes and various properties. Some common ontologies and vocabularies we use are: SKOS, schema.org, DCAT, and DC Terms.[5][9][10][11]


## What are Controlled Vocabularies and why are they useful?

Controlled vocabularies are a set of agreed terms within an application, project, or community. They enable data discovery, provide clear and unambiguous terms with definitions, and can be reused by others. Linked-Data-enabled controlled vocabularies with persistent URI's enable long-lived terms on the internet that are understood by both humans and machines. TERN uses controlled vocabularies for anything that requires a set of controlled lists to either tag data or drive applications such as a data access portal. 



## What is SKOS?

The Simple Knowledge Organization System (SKOS) is a data model to describe thesauri, classification schemes, taxonomies, and controlled vocabularies.[4][5] For example, a classification scheme on the type of vehicle in a controlled list like `['car', 'motorcycle', 'jetski', 'boat']` should not be defined in application code, rather, it should be defined in RDF with SKOS, and fetched from the point-of-truth (or a downstream application containing the same data as the point-of-truth). 


## What is Git?

Git is a version control system for tracking source code in software development. TERN will use Git to track the RDF data on Bitbucket. This repository will be the *"master"* Git repository with pointers to each sub-repository of RDF data such as CORVEG Vocabularies, AusPlots Rangelands Vocabularies, Organisations and Persons, etc. Each sub-repository will be a Git submodule. 

The purpose of using Git submodules is to have the user (you) recursively clone the parent repository, including all the pointers to a submodule. Submodules will give us the flexibility in having separate CI/CD workflows to clean, validate, test, and finally deploy to the triplestore. Without Git submodules (as in having a monolithic repository), a small change in e.g. the CORVEG vocabularies will trigger the entire workflow to clean, validate, test, and deploy to the triplestore. This is very inefficient. 

![Git submodule diagram](docs/img/knowledge-graph-submodule-diagram.png)


## What is TopBraid Composer?

TERN uses the TopBraid Composer (Free Edition) to create and modify new controlled vocabularies or data about persons or organisations, or other things. Coupled with a small *profile* of an ontology (or ontologies) and SHACL shapes, TopBraid is capable of generating custom forms and built-in validation for data entry. 


## What is SHACL?

The Shapes Constraint Language (SHACL) is a language for defining constraints against the shape of a graph. Remember how RDF statements are inter-connected with each other through relationships? SHACL allows us to define a set of constraints on a *target class* such as the *predicates* it is allowed to have and the *range values* allowed for those predicates. 


## What is a Triplestore?

A triplestore is a database to store sets of triples. This is synonymous to a relational database storing tables and rows. SQL can be used to query a relational database and retrieve or update data. Triplestores also have a query language to retrieve or update data using SPARQL. A bonus with triplestores is that a HTTP endpoint is usually created *"for free"* to allow data access from downstream applications. 


## What is SPARQL?

SPARQL 1.1 [7] is a query language designed to query RDF data. Applications can make queries to a triplestore by submitting SPARQL queries to a triplestore's SPARQL endpoint. SPARQL 1.1 supports retrieving RDF triples with various query results. SPARQL Update 1.1 [8] allows for modifications to the RDF data in a triplestore. 


## What is Linked Data?

*Details coming soon...*



# Governance

1. Editors will clone this repository and its sub-repository to make modifications. 
2. Editors must create their own working branch to save changes. Make sure the new branch is in the intended sub-repository and not the parent repository. 
3. Editing must be done using the TopBraid Composer Free Edition ([download page](https://www.topquadrant.com/topbraid-composer-install/)). 
4. Each push will run pipeline steps for validation on the data and the schema to ensure conformance. Failed pipeline steps will prohibit the pipeline in progressing further. 
5. Once a task is completed, the editor will make a pull request for the changes in their branch to be merged into the **staging** branch. 
6. An administrator will review the content and if everything is good, the changes will be merged. 
7. The data will be deployed to the staging database for user testing. 
8. Once the editor is happy that their data is functioning correctly with downstream applications in the staging database, they can then create another pull request to merge changes from the staging branch into the **master** branch. 
9. Again an administrator will review and if everything is good, the changes will be merged. 

![Activity diagram](docs/img/knowledge_graph_activity.png)


# User Guide

## Getting Started

Clone the parent repository
```
git clone https://edmondchuc@bitbucket.org/terndatateam/knowledge-graph-data.git
```

Change directory 

Enable submodules
```
git submodule init
git submodule update
```

If you were to create a new person in the persons list, change directory into `org-and-person`.
```
cd org-and-person
```

You are now in the Git submodule (sub-repository). Commits you make here will not affect the parent repository. 

Before you start editing, first create your own working branch. Let's see what branch we are on first.
```
git branch
```

You can see with the asterix, we are currently on the master branch. 
```
org-and-person uqechuc$ git branch
* master
```

Let's create and switch to a new branch.
```
# This will create a new branch called 'edmond' and switch to it from the current branch. 
git checkout -b edmond
```

If your working branch already exists, simply switch to it.
```
git checkout edmond
```

Let's see what branch we are on now.
```
org-and-person uqechuc$ git branch
* edmond
  master
```

If you switched to the working branch without creating it, make sure you do a `git pull` to get the latest updates from the remote branch. If all is up to date, you can go back to the master branch, also do a `git pull` and come back to your working branch. Then perform a `git merge`, merging the contents of master into your branch.
```
git merge master
```

This will reduce the likelyhood of merge conflicts later when you make a pull request to merge into master. 

Now that you are on a working branch that is not master, feel free to make changes.

Commit your changes as you would normally and push them to your remote branch. The first time you push, the remote branch would not exist yet. Follow the on-screen prompts to push to your remote branch. 


## Check Pipeline Validation

Going to the Pipelines tab on the left hand side of the Bitbucket repository will show the pipelines. Make sure you are in the sub-repository and not the parent to see the correct pipeline details. You should see that your push to your remote branch had triggered a pipeline. If the status failed, contact an administrator to find out why. If all was successful, you can now proceed to make a pull request to get your changes merged into master. 


## Pull Request for Merges

In Bitbucket, there is a Pull requests tab on the left hand side. Create a new pull request and ensure the branch you are merging in to staging is your remote branch. Creating a pull request will send an email notification to the administrators. The administrators will go through a checklist to ensure the changes are appropriate. If the administrators take too long to reply, send them an aggressive email or Slack message to get things moving. 

The above step will make a pull request and deploy to staging. If you want to request the data in staging to be available in master (production), then make another pull request, but this time select merging from staging to master. 


# Adminstrator Notes

## Adding a New Submodule

Create a new Bitbucket repository under the "TERN Data Team" and in the project "RDF Graph Store". Give the repository a sensible name following the conventions of the other existing repositories. 

Clone the new repository somewhere temporary in our local system. We then push it to the remote master branch with an initial commit (e.g. adding `README.md`). Once that's done, we can now link from this parent repository to the submodule by running:
```
# Please remove the username in the repository URL
# https://edmondchuc@bitbucket.org/terndatateam/corveg-cv.git
# to
# https://bitbucket.org/terndatateam/corveg-cv.git

git submodule add <git-url>
```

Now that we have altered the local copy of the parent repository by adding the new submodule, we need to commit the changes and push it to the remote branch. 

Once pushed, anyone cloning this parent repository should now get the new submodule as well. 

Create branches and push to remote repository. Branches should include *master* and *staging*. Now in Bitbucket, set up the branch model, assigning the development branch to staging and the production branch to master. Now set the branch permissions for these two branches, allowing no direct write access except for merges based on pull requests by the administrators. 

Once merges have been set up, it is a good idea to set up the new TopBraid RDF file by importing the required ontologies/shape files. Enter some data and then set up the Pipeline by reusing the existing configuration files from the other existing submodules. Pipelines requires enabling in the repository's settings.


## Configuring Bitbucket Repository

Create the staging branch. Push the content to the master and staging branch. 

In settings, change the branching model to point the **development branch** to staging and point the **production branch** to master. 

Create branch permissions for both the staging branch and the master branch. Set it to no write access for both branches and allow merge via pull request to authorised administrators. In the *merge checks* section of the branch permissions, tick the check for at least 1 successful build and no failed builds. 

Enable Pipelines in the Bitbucket repository.


### Submodule Compulsory Files

A submodule must have a Bitbucket Pipelines configured. Therefore there must exist a `bitbucket-pipelines.yml` file. Copy the contents of an existing `bitbucket-pipelines.yml` file from another repository and make the necessary changes in the `&validate-shacl` step. No changes need to be made for both the staging or master pipelines. 

 A submodule must also have `ingest_config_staging.yml` and `ingest_config_master.yml` files to describe how and where the data should be deployed. Copy and existing ingest file from a repository and make changes to the configuration such as which named graph the data should be uploaded to. 


## Commands

- Adding a submodule: `git submodule add <git-url>`
- Initiate a submodule: `git submodule init`
- Update a submodule: `git submodule update`
- To remove a submodule, see: https://gist.github.com/myusuf3/7f645819ded92bda6677


## Bitbucket Workflow

For a given sub-repository, go to `Settings -> WORKFLOW -> Branch permissions` and set it so that there is no direct write access to the master or staging branch, and only the administrators can merge via a pull request. 

All repositories will use Bitbucket Pipelines as the CI/CD solution. 

Users create and edit on their own branch. A pull request is issued to get their changes into the master branch. When a pull request is initiated, validation of the data will be performed automatically by Pipelines. If any validations fail, the user will have to fix the errors before making another pull request. 

Pull requests are only merged into the master branch once data has been validated and the content has been reviewed. A merge will result in a push to the master branch, which will trigger Pipelines to deploy the data to the triplestore, and other possible destinations. 

## Merge Conflicts

If there are merge conflicts when approving pull requests, then you must manually fix the merge conflicts locally. Create a branch called `resolve-conflicts` and merge the conflicts from the two branches. Fix the merge conflicts, push to remote branch, and make another pull request with the fixed merge into staging/master. 


# Common Issues

## When I do a 'git push' in a submodule, it is asking me for the password to a Bitbucket account that is not mine.
### Reason
For some reason, the `git submodule update` command clones the submodules with the URL link of the original author (e.g. git clone https://edmondchuc@bitbucket.org/terndatateam/ausplots-rangelands-cv.git). Now it will always prompt for the password of the Bitbucket user `edmondchuc`. 

### Solution
What we need to do is set the URL of the repository to our own user (or omit the user all together) by issuing the following command:
```
git remote set-url origin clone https://bitbucket.org/terndatateam/ausplots-rangelands-cv.git
```

## URL link to the raw file of a SKOS vocabulary in Bitbucket is tied to a specific link

### Example
The following link is tied to the commit hash `d1d6f691f41b67dce51d8811d94abac693c482b5` https://bitbucket.org/terndatateam/ausplots-rangelands-cv/raw/d1d6f691f41b67dce51d8811d94abac693c482b5/data.ttl

### Solution
Point the raw file's URL to the `HEAD` of the repository's default branch: 
https://bitbucket.org/terndatateam/ausplots-rangelands-cv/raw/HEAD/data.ttl


# Contact

**Edmond Chuc**  
*Software engineer*  
[e.chuc@uq.edu.au](mailto:e.chuc@uq.edu.au)  


**Andrew Cleland**  
*Software engineer*  
[a.cleland@uq.edu.au](mailto:a.cleland@uq.edu.au)  


# References

## RDF, Linked Data, and the Semantic Web

- [1] [RDF Primer](https://www.w3.org/TR/rdf-primer/)
- [2] [RDF 1.1 Concepts and Abstract Syntax - W3C Recommendation](https://www.w3.org/TR/rdf11-concepts/)
- [3] [RDF Schema 1.1 - W3C Recommendation](https://www.w3.org/TR/rdf-schema/)
- [4] [SKOS Primer](https://www.w3.org/TR/skos-primer/)
- [5] [SKOS Reference - W3C Recommendation](https://www.w3.org/TR/skos-primer/)
- [6] [Shapes Constraint Language (SHACL) - W3C Recommendation](https://www.w3.org/TR/shacl/)
- [7] [SPARQL 1.1 Query Language - W3C Recommendation](https://www.w3.org/TR/sparql11-query/)
- [8] [SPARQL 1.1 Update - W3C Recommendation](https://www.w3.org/TR/sparql11-update/)
- [9] [Data Catalog Vocabulary (DCAT) - W3C Recommendation](https://www.w3.org/TR/vocab-dcat/)
- [10] [Dublin Core Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/)
- [11] [schema.org](https://schema.org/)
- [12] [CURIE Syntax 1.0](https://www.w3.org/TR/curie/)
- [13] [Uniform Resource Identifier](https://en.wikipedia.org/wiki/Uniform_Resource_Identifier)

## Git

- [Git](https://git-scm.com/)
- [Git Tools - Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules)
- [Git Submodules basic explanation](https://gist.github.com/gitaarik/8735255)
- [Git Submodules vs Git Subtrees](https://codewinsarguments.co/2016/05/01/git-submodules-vs-git-subtrees/)
- [Learn Git in 15 Minutes (YouTube)](https://www.youtube.com/watch?v=USjZcfj8yxE)