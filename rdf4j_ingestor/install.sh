#!/usr/bin/env bash

mkdir -p ~/bin
export PATH="$HOME/bin:$PATH"

chmod 744 ingest
cp ingest ~/bin

pip install -r requirements.txt