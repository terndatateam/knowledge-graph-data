# RDF Validator

## Turtle Validator

Install by running 

```
source install.sh
```

To validate a file, run

```
turtle_validator <filename>
```

