#!/usr/bin/env bash

chmod 744 turtle_validator
chmod 744 validate_all_files
mkdir -p ~/bin
cp turtle_validator ~/bin
cp validate_all_files ~/bin
export PATH="$HOME/bin:$PATH"
pip install -r requirements.txt